const {user_game, user_game_biodata, user_game_history} = require('../models');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const {JWT_SIGNATURE_KEY} = process.env;

module.exports = {
    login: async (req, res, next) => {
        try {
            const {username, password} = req.body;
            const user = await user_game.findOne({where:{username: username}});
            if(!user) {
                return res.status(400).json({
                    status: false,
                    message: 'User not found!'
                });
            }

            const passwordCorrect = await bcrypt.compare(password, user.password);
            if(!passwordCorrect) {
                return res.status(400).json({
                    status: false,
                    message: 'Wrong username or password!'
                });
            }

            let payload = {
                id: user.id,
                username: user.username
            }

            const token = jwt.sign(payload, JWT_SIGNATURE_KEY);

            return res.status(200).json({
                status: true,
                message: 'login success',
                data: {
                    token: token
                }
            });
        } catch (err) {
            next(err);
        }
    }
}
const express = require('express');
const router = express.Router();
const controller = require('../controllers');
const middleware = require('../helpers/middleware');

router.get('/', middleware.mustLogin, controller.userGameBiodata.index);
router.get('/:userBiodataId', middleware.mustLogin, controller.userGameBiodata.show);
router.post('/', middleware.mustLogin, controller.userGameBiodata.create);
router.put('/:userBiodataId', middleware.mustLogin, controller.userGameBiodata.update);
router.delete('/:userBiodataId', middleware.mustLogin, controller.userGameBiodata.delete);

module.exports = router;
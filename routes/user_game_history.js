const express = require('express');
const router = express.Router();
const controller = require('../controllers');
const middleware = require('../helpers/middleware');

router.get('/', middleware.mustLogin, controller.userGameHistory.index);
router.get('/:historyId', middleware.mustLogin, controller.userGameHistory.show);
router.post('/', middleware.mustLogin, controller.userGameHistory.create);
router.put('/:historyId', middleware.mustLogin, controller.userGameHistory.update);
router.delete('/:historyId', middleware.mustLogin, controller.userGameHistory.delete);

module.exports = router;